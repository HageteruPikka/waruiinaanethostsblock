# Hosts file block by warui.intaa.net

There is a famous hostsblock suite, https://github.com/gaenserich/hostsblock . However,
this is too much for me. I rather want a simple something just for me. I am not a
programmer but I like DIY. If you also like DIY, let's check and do it yourself.

https://warui.intaa.net/adhosts/ provides several hosts files for ad blocking. The script,
``warui-hosts``, helps to 

- download a hosts file.
- remove domains by ``whitelist``.
- add ``127.0.0.1 localhost``
- copy the hosts file to ``/etc/hosts.block`` .

When the hosts file prevents your browsing, ``toggle-waruihosts`` helps to toggle using
the blocking hosts file, ``etc/hosts.block``, and another one, ``/etc/hosts.unblock``. You
need preparing the ``hosts.unblock``.

They are simple scripts. Thus, you can read them and customize to your environment.


# 悪いインターネットさん提供の hosts ファイルを使ったホストブロック

https://github.com/gaenserich/hostsblock の挙動が良くわからなくて、要は
https://warui.intaa.net/adhosts/ から hosts ファイルを頂戴してくるんだから、自分専用の簡単
な何かなら、書けるんじゃないかと考えて書いてます。DIY が好きな人の参考にでもなれば。いやプ
ログラマーでもない人間の書いたものなんて参考にはならないかな。まあいいです。

``warui-hosts`` は

- 悪いインターネットさんから hosts を取ってきて
- ``whitelist`` に書いてある domain を削除して
- ``127.0.0.1 localhost`` を追加して
- ``black.txt`` をコピペして
- ``/etc/hosts.block`` にコピーする。

ブロックしすぎてブラウジングに支障が出たときに、``/etc/hosts.block`` と
``/etc/hosts.unblock`` を切りかえる ``toggle-waruihosts`` も用意。``/etc/hosts.unblock``
は自分で書いておいてください。

何にせよスクリプトみて自分好みに書き変えてください。
